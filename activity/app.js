let users = `[
      {
         "id": 1,
		 "firstName":"Juan",
		 "lastName":"Del Cruz", 
		 "email":"sample@email.com",
		 "password":"1234",
		 "isAdmin": true,
		 "mobileNumber":"09981234234"		   
      },
      {
         "id": 2,
		 "firstName":"Jane",
		 "lastName":"Aquino", 
		 "email":"jane@email.com",
		 "password":"1234565",
		 "isAdmin": false,
		 "mobileNumber":"09983455644"		   
      },
      {
         "id": 3,
		 "firstName":"Pedro",
		 "lastName":"Penduco", 
		 "email":"pedro@email.com",
		 "password":"1234565dddd",
		 "isAdmin": false,
		 "mobileNumber":"09983444434"		   
      }
]`;

let orders = `[
	{
		"userID": "2",
		"transDate": "Jan 15, 2021",
		"status": "delivered",
		"total": "1,784.76"
	},
	{
		"userID": "3",
		"transDate": "May 23, 2021",
		"status": "delivered",
		"total": "567.00"
	}
]`;

let products = `[
	{
		"name": "Super Bawang",
		"description": "garlic snack",
		"price": "5",
		"stocks": 100,
		"isActive": true,
		"sku": "D233sDDF"
	},
	{
		"name": "Bazooka",
		"description": "bubble gum",
		"price": "2",
		"stocks": 100,
		"isActive": true,
		"sku": "123Gd231"
	},
	{
		"name": "Tarzan",
		"description": "candy",
		"price": "5",
		"stocks": 0,
		"isActive": false,
		"sku": "gH69Jj1"
	}
]`;

let orderProducts = `[
	{
		"orderID": 1,
		"productID": "50",
		"quantity": 10,
		"price": 5,
		"subTotal": 50
	},
	{
		"orderID": 2,
		"productID": "60",
		"quantity": 10,
		"price": 2,
		"subTotal": 20 
	},
	{
		"orderID": 3,
		"productID": "70",
		"quantity": 10,
		"price": 3,
		"subTotal": 30
	}
]`

// let arrUsers = JSON.parse(orderProducts);
// arrUsers.forEach(element => {
//   console.log(element);
// });